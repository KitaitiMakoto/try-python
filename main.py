import sys
import json
import csv
from google.cloud import bigquery

with open('members.schema.json') as f:
    SCHEMA = json.load(f)

FIELDS = [field['name'] for field in SCHEMA]
DATETIME_FIELDS = [field['name'] for field in SCHEMA if field['type'] == 'DATETIME']

CSV_FILE = 'members.csv'

TABLE_ID = 'epub-searcher.sample_members.members'

def main():
    extract()
    transform()
    load()

def extract():
    print('extract', file=sys.stderr)

def transform():
    print('transform', file=sys.stderr)

    with open('./members.json') as source:
        members = json.load(source)
    with open(CSV_FILE, 'w') as dest_file:
        dest = csv.DictWriter(dest_file, FIELDS)
        dest.writeheader()
        for member in list(members):
            normalize_member(member)
            dest.writerow(member)

def load():
    print('load', file=sys.stderr)

    client = bigquery.Client()
    config = bigquery.LoadJobConfig(
        source_format=bigquery.SourceFormat.CSV,
        skip_leading_rows=1,
        write_disposition='WRITE_TRUNCATE'
    )
    with open(CSV_FILE, "rb") as source:
        job = client.load_table_from_file(source, TABLE_ID, job_config=config)
    job.result()

def normalize_member(member):
    fields = list(member.keys())
    for field in fields:
        if field in FIELDS:
            if field in DATETIME_FIELDS and member[field]:
                member[field] = member[field][:19]
        else:
            del member[field]

main()
